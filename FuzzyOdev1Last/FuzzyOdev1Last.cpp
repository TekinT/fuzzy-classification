// FuzzyOdev1Last.cpp : Defines the entry point for the console application.
//


#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include "Behaviour.h"
#include <string>


using namespace std;


int main()
{
	Behaviour matchingBehaviour;
	matchingBehaviour.ruleBase.print();
	float width=0, length=0;
	string line,flowerRealName;
	ifstream myfile("iris.data");

	if (myfile.is_open())
	{
		int linecounter = 1;
		getline(myfile, line);//Put away header line
		int intErrorCount=0;
		while (getline(myfile, line))
		{
			string token;
			istringstream istr(line);
			getline(istr, token, ',');//sepal length
			getline(istr, token, ',');//sepal width
			getline(istr, token, ','); length = std::stof(token);//Petal length
			getline(istr, token, ','); width  = std::stof(token);//Petal width
			getline(istr, token, ','); flowerRealName = token;//Flower realname
			cout  << "----------------------------------------------"<<  linecounter++<<endl;
			cout << "Program Input: width=" << width << " length=" << length << endl;
			matchingBehaviour.membershipTest(width, length, flowerRealName,&intErrorCount);
		}
		myfile.close();
		cout << "____________________________________" << endl;
		cout << "There are " << intErrorCount << " errors out of 150 data." << endl;
		cout << "Error rate is: " << (intErrorCount / 150.0 * 100) << "%" << endl;
		cout << "Success rate: "<< 100-(intErrorCount / 150.0 * 100) << "%" << endl;
	}
	else cout << "Unable to open file";
	
	std::cin >> length;//Pause until input and enter
	return 0;
}
//It gives wrong answer for w=2.29 and L=6.9

//Total errors are 4 out of 150 which is 2.66%
//Errors cannot be solved with the given information. Since they are both in the versicolor and virginica region.
//Additionally the information of the other leaf cannot solve the problem, since its membership functions give the same value for those errors.
//70	5.6, 2.5, 3.9, 1.1, Iris - versicolor		   	e
//77	6.8, 2.8, 4.8, 1.4, Iris - versicolor			e
//83	5.8, 2.7, 3.9, 1.2, Iris - versicolor			e
//119	7.7, 2.6, 6.9, 2.3, Iris - virginica			e Sepal info could help for segmentation of this flower



