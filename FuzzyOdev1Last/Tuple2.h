
#ifndef TUPLE2_H
#define TUPLE2_H

class Tuple2
{
public:
	Tuple2();
	Tuple2(int k1, int k2);
	bool operator<(const Tuple2 &right) const;
	int key1, key2;
};

#endif

