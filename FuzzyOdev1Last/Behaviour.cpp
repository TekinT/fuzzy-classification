#include "Behaviour.h"
#include <algorithm>
#include <string>

Behaviour::Behaviour()
{
    
    //Petal length => pl
	//low represents setosa
    pl_lowTrapezoid.a = 0.9;
    pl_lowTrapezoid.b = 1.1;
    pl_lowTrapezoid.c = 1.8;
    pl_lowTrapezoid.d = 2.0;

	//Medium represents versicolor
    pl_mediumTrapezoid.a = 2.9;
    pl_mediumTrapezoid.b = 3.1;
    pl_mediumTrapezoid.c = 5.0;//4.9 made it worse
    pl_mediumTrapezoid.d = 5.2;

	//High represents virginica
    pl_highTrapezoid.a = 4.3;//changed from 4.4 3rd
    pl_highTrapezoid.b = 4.4;//changed from 4.6 3rd
    pl_highTrapezoid.c = 6.9;//changed from 6.8
    pl_highTrapezoid.d = 7.0;//changed from 6.10

    //Petal width=pw, low represents setosa
    pw_lowTrapezoid.a = 0.0;
    pw_lowTrapezoid.b = 0.2;
    pw_lowTrapezoid.c = 0.5;
    pw_lowTrapezoid.d = 0.7;

	//Medium represents versicolor
    pw_mediumTrapezoid.a = 0.9;
    pw_mediumTrapezoid.b = 1.1;
    pw_mediumTrapezoid.c = 1.7;
    pw_mediumTrapezoid.d = 1.9;

	//High represents virginica
    pw_highTrapezoid.a = 1.3;
    pw_highTrapezoid.b = 1.5;
    pw_highTrapezoid.c = 2.4;
    pw_highTrapezoid.d = 2.6;

	ruleBase.setName("Classification model");

    // add rules
    // read sample rule 1 as if width is LOW and height is LOW then the type is SETOSA
	
	// note that sample rules may not be correct.

    // LOW
	//We have three rules. Each type of flower has its own membership functions 
    ruleBase.addRule(Rule(LOW, LOW, SETOSA));    //IF petal length is low AND petal width is low THEN flower is SETOSA
    ruleBase.addRule(Rule(MED, MED, VERSICOLOR));//IF petal length is MEDIUM AND petal width is MEDIUM THEN flower is VERSICOLOR
    ruleBase.addRule(Rule(HIGH,HIGH, VIRGINICA));//IF petal length is high AND petal width is high THEN flower is VIRGINICA

    ruleBase.print();
	
	
	// fuzzy membership values for input parameters
    int ruleBaseSize =  3;
    pl_distanceValues = (double *)malloc(ruleBaseSize*sizeof(double));//T_ keeps the memberfunction output values that are calculated for each inputs
    pw_distanceValues = (double *)malloc(ruleBaseSize*sizeof(double));//T_ keeps the memberfunction output values that are calculated for each inputs
}

void Behaviour::membershipTest(float width, float length, string argFlowerRealName, int * argErrorCount)
{
    // clear all fuzzy membership values (for now we have low, med and high for width and height so 3 x 3 = 9 locations)
	for(int i=0;i<10;i++)
	{
		membershipValues[i] = 0.0;
	}

    // clear previous bolFire strengths
    ruleBase.clearbolFires();

    // compute all membership functions for all combinations of LOW MEDIUM and HIGH
	
    // calculating Petal length membership functions
	pl_lowTrapezoid.CalcValue(length, &pl_distanceValues[LOW]);
	pl_mediumTrapezoid.CalcValue(length, &pl_distanceValues[MED]);
	pl_highTrapezoid.CalcValue(length, &pl_distanceValues[HIGH]);


	//Petal Width
	pw_lowTrapezoid.CalcValue(width,&pw_distanceValues[LOW]);
	pw_mediumTrapezoid.CalcValue(width,&pw_distanceValues[MED]);
	pw_highTrapezoid.CalcValue(width,&pw_distanceValues[HIGH]);

	//Here all rules are fired. First the fire strength is calculated for each rule. Then the rule with the maximum strength is found.
    std::pair<int,double> intPairMax = std::make_pair(0,0);//<index,value>
    for(int i=LOW; i<=HIGH; i++)
    {
        ruleBase.ruleMap[i].doubleFireStrength = std::min(pl_distanceValues[i],pw_distanceValues[i]);
        if(ruleBase.ruleMap[i].doubleFireStrength > 0.0)
        {
            ruleBase.ruleMap[i].bolFire = true;
            if(intPairMax.second<ruleBase.ruleMap[i].doubleFireStrength)
            {
                intPairMax.first=i;
                intPairMax.second=ruleBase.ruleMap[i].doubleFireStrength;
            }
        }
        else
        {
            ruleBase.ruleMap[i].bolFire = false;
        }
    }

    //Stores the pair which has the maximum bolFire strength
    //Conseuent of the rule with maximum strength is printed as the result.
    int matchingResult = ruleBase.ruleMap[intPairMax.first].intConsequent;


    string matchingResultName = ruleBase.ruleMap[intPairMax.first].strConsequentName;
	
	std::cout << "Program output: " << matchingResultName << endl;
	std::cout << "True    Output: " << argFlowerRealName << endl;
	if (matchingResultName != argFlowerRealName)
	{
		(*argErrorCount)++;
		cout <<"*******************"<< "Error #" << (*argErrorCount) << "*******************" << endl;
	}
	else
	{

	}
}
