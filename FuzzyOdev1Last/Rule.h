#ifndef RULE_H
#define RULE_H

#include "GenericHeader.h"

class Rule
{
    public:
        Rule();
        Rule(int ant1, int ant2, int intArgConsequent);

        int intAntecedent1;  // fuzzy variable to store linear motions
        int intAntecedent2;  // fuzzy variable to store linear motions
        string strAntName1;
        string strAntName2;

        int intConsequent; // fuzzy variable to store motion model
        std::string strConsequentName;

        bool bolFire; // variable to decide whether the rule fired or not
        double doubleFireStrength;

};

#endif // MOTIONRULE_H
