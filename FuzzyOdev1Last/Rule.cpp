#include "Rule.h"
Rule::Rule()
{
    intAntecedent1 = -1;
    intAntecedent2 = -1;

    intConsequent = -1;

    strAntName1 = UNDF;
    strConsequentName = UNDF;
}
// I think it is just for keeping the rule not for calculating it
Rule::Rule(int ant1, int ant2, int intArgConsequent)
{
    intAntecedent1 = ant1;
    switch (intAntecedent1)
	{
	case LOW:
        strAntName1 = LOWs;
		break;
    case MED:
        strAntName1 = MEDs;
		break;
    case HIGH:
        strAntName1 = HIGHs;
		break;
	}

    intAntecedent2 = ant2;
    switch (intAntecedent2)
    {
    case LOW:
        strAntName2 = LOWs;
        break;
    case MED:
        strAntName2 = MEDs;
        break;
    case HIGH:
        strAntName2 = HIGHs;
        break;
    }

    intConsequent = intArgConsequent;
    switch (intConsequent)
	{
        case SETOSA:
            strConsequentName = SETOSAs;
            break;
        case VERSICOLOR:
            strConsequentName = VERSICOLORs;
            break;
		case VIRGINICA:
            strConsequentName = VIRGINICAs;
            break;
	}



    // this becomes true when the rule bolFires
    bolFire = false;
    doubleFireStrength = 0.0;
}
