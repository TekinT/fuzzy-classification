#ifndef GENERICHEADER_H
#define	GENERICHEADER_H

#include <iostream>
#include <map>
#include <list>
#include <vector>

#include <stdlib.h>
#include <stdio.h>
#include <string>


using namespace std;


// definitions for fuzzy sets
// inputs
#define LOW 0
#define MED 1
#define HIGH 2

// output strings
#define LOWs "LOW"
#define MEDs "MEDIUM"
#define HIGHs "HIGH"


// outputs
#define SETOSA 0
#define VERSICOLOR 1
#define VIRGINICA 2

// output strings
#define SETOSAs "Iris-setosa"
#define VERSICOLORs "Iris-versicolor"
#define VIRGINICAs "Iris-virginica"

#define UNDF "UNDEFINED"




#endif	/* GENERICHEADER_H */

