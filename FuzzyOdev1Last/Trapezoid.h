#ifndef TRAPEZOID_H
#define TRAPEZOID_H


class Trapezoid
{
    public:
        Trapezoid();
        
		//calculates the alpha cut of trapezoid
		void CalcValue(double in, double * out);
		
        double a,b,c,d;
};




#endif // TRAPEZOID_H
