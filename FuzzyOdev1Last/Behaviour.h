#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H

#include "GenericHeader.h"
#include "RuleBase.h"
#include "Trapezoid.h"


class Behaviour
{
    public:
        Behaviour();

        void membershipTest(float width, float length,string argFlowerRealName, int * argErrorCount);


        // 9 element array to store low-medium-high membership values for width and height
        double * membershipValues = (double *)malloc(9*sizeof(double));

        RuleBase ruleBase;

        // trapezoids for input fuzzy variables
		Trapezoid sl_lowTrapezoid;//Sepal length
        Trapezoid sl_mediumTrapezoid;
        Trapezoid sl_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid sw_lowTrapezoid;//Sepal width
        Trapezoid sw_mediumTrapezoid;
        Trapezoid sw_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid pl_lowTrapezoid;//Petal length
        Trapezoid pl_mediumTrapezoid;
        Trapezoid pl_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid pw_lowTrapezoid;//Petal width
        Trapezoid pw_mediumTrapezoid;
        Trapezoid pw_highTrapezoid;

        int result;
        string resultString;
        double * pl_distanceValues;
        double * pw_distanceValues;

};

#endif // BEHAVIOUR_H
