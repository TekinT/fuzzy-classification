# README #
2016\04\25

This is the program which we developed as a course project. This program uses Fuzzy to classify flowers according to their leaf information given from the input. The name of the flower is Iris and it has two leaves that can be used for determining the flower's type. Leaves' width and height are the criterias used in classification.