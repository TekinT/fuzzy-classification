#include "Tuple2.h"

Tuple2::Tuple2()
{

}

Tuple2::Tuple2(int k1, int k2)
{
	key1 = k1;
	key2 = k2; 	
}

bool Tuple2:: operator<(const Tuple2 &right) const
{
	if ( key1 == right.key1 ) 
	{
		if ( key2 == right.key2 )
			return true;
		else
			return key2 < right.key2;
     }
     else
		 return key1 < right.key1;        

}



