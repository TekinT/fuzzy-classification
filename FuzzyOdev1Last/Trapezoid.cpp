#include "Trapezoid.h"


Trapezoid::Trapezoid()
{
	a=b=c=d=0.0;
}

//This method calculates the membership value for inputs
void Trapezoid::CalcValue(double in, double * out)
{
	if (in <= a)
	{
		*out = 0;
		return;
	}
	if (in > a && in < b)
	{
		*out = 1 / (b - a)*(in - a);
		return;
	}
	if (in >= b && in <= c)
	{
		*out = 1;
		return;
	}
	if (in>c && in<d)
	{
		*out = 1 / (c - d)*(in - d);
		return;
	}
	if (in >= d)
	{
		*out = 0;
		return;
	}
}

/*Trapezoid::Trapezoid(double aV, double bV, double cV, double dV)
{
	a = aV;
	b = bV;
	c = cV;
	d = dV;
}*/
