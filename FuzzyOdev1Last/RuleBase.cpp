#include "RuleBase.h"
RuleBase::RuleBase()
{
    name = "";
}

void RuleBase::setName(std::string n)
{
	name = n;
}

void RuleBase::addRule(Rule r)
{
    int key1 = r.intAntecedent1;
    ruleMap[key1]=r;
}

void RuleBase::clearbolFires()
{

	for(int i=0;i<ruleMap.size();i++)
	{
		ruleMap[i].bolFire = false;
		ruleMap[i].doubleFireStrength = 0.0;
	}

}

void RuleBase::clear()
{
	ruleMap.clear();
}

void RuleBase::print()
{
	printf("------------------------------------------\n");
	printf("%s Rule Base:\n", name.c_str());

	for(my_it =ruleMap.begin(); my_it !=ruleMap.end(); my_it++)
        printf("(%s): (%s)\n", my_it->second.strAntName1.c_str(), my_it->second.strConsequentName.c_str());

	printf("------------------------------------------\n");
}

void RuleBase::printbolFired()
{
	printf("------------------------------------------\n");
	for(my_it =ruleMap.begin(); my_it !=ruleMap.end(); my_it++)
        if(my_it->second.bolFire == true)
            printf("(%s): (%s) -> bolFire strength: %.2f\n", my_it->second.strAntName1.c_str(), my_it->second.strConsequentName.c_str(), my_it->second.doubleFireStrength);
	printf("------------------------------------------\n");
}
