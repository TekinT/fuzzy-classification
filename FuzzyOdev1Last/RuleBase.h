#ifndef RULEBASE_H
#define RULEBASE_H

#include "GenericHeader.h"
#include "Rule.h"

class RuleBase
{
    public:
        RuleBase();
        std::map<int, Rule> ruleMap; // has three keys
        std::map<int, Rule>::iterator my_it;

        std::string name; // name of the rulebase

        void print();
        void printbolFired();
        void setName(std::string n);
        void addRule(Rule mr);
        void clearbolFires();
        void clear();
};

#endif // MOTIONRULEBASE_H

