*********************MAIN.CPP**********************
#include <iostream>
#include <stdio.h>

#include "Behaviour.h"


using namespace std;


int main()
{
    MatchingBehaviour matchingBehaviour;
    matchingBehaviour.ruleBase.print();


    matchingBehaviour.membershipTest(width, height);


    return 0;
}
*********************Rule.h*********************
#ifndef RULE_H
#define RULE_H

#include "GenericHeader.h"

class Rule
{
    public:
        Rule();
        Rule(int ant1, int ant2, int cons);

        int antecedent;  // fuzzy variable to store linear motions

        string antName;

        int consequent; // fuzzy variable to store motion model
        std::string consName;

        bool fire; // variable to decide whether the rule fired or not
        double fireStrength;

};

#endif // MOTIONRULE_H

*********************Rule.cpp*********************
#include "Rule.h"

Rule::Rule()
{
    antecedent1 = -1;
    antecedent2 = -1;

    consequent = -1;

    antName = UNDF;
    consName = UNDF;
}

Rule::Rule(int ant1, int ant2, int cons)
{
    antecedent = ant;
    switch (antecedent)
    {
    case LOW:
        antName = LOWs;
        break;
    case MED:
        antName = MEDs;
        break;
    case HIGH:
        antName = HIGHs;
        break;
    }

    consequent = cons;
    switch (consequent)
    {
        case SETOSA:
            consName = SETOSAs;
            break;
        case VERSICOLOR:
            consName = VERSICOLORs;
            break;
        case VIRGINICA:
            consName = VIRGINICAs;
            break;
    }



    // this becomes true when the rule fires
    fire = false;
    fireStrength = 0.0;
}

*********************Rulebase.h*********************
#ifndef MATCHINGRULEBASE_H
#define MATCHINGRULEBASE_H

#include "GenericHeader.h"
#include "MatchingRule.h"

class MatchingRuleBase
{
    public:
        MatchingRuleBase();
        std::map<int, MatchingRule> ruleMap; // has three keys
        std::map<int,  MatchingRule>::iterator it;
ruleMap.
        std::string name; // name of the rulebase

        void print();
        void printFired();
        void setName(std::string n);
        void addRule( MatchingRule mr);
        void clearFires();
        void clear();
};

#endif // MOTIONRULEBASE_H
*********************Rulebase.cpp*********************
#include "RuleBase.h"

RuleBase::RuleBase()
{
    name = "";
}

void RuleBase::setName(std::string n)
{
    name = n;
}

voidRuleBase::addRule(MatchingRule r)
{
    int key = r.antecedent;
    ruleMap[key]=r;
}

void MatchingRuleBase::clearFires()
{
    for(it=ruleMap.begin();it!=ruleMap.end();it++)
    {
        it->second.fire = false;
        it->second.fireStrength = 0.0;
    }
}

void RuleBase::clear()
{
    ruleMap.clear();
}

void RuleBase::print()
{
    printf("------------------------------------------\n");
    printf("%s Rule Base:\n", name.c_str());

    for(it=ruleMap.begin();it!=ruleMap.end();it++)
        printf("(%s): (%s)\n", it->second.antName.c_str(), it->second.consName.c_str());

    printf("------------------------------------------\n");
}

void RuleBase::printFired()
{
    printf("------------------------------------------\n");
    for(it=ruleMap.begin();it!=ruleMap.end();it++)
        if(it->second.fire == true)
            printf("(%s): (%s) -> fire strength: %.2f\n", it->second.antName.c_str(), it->second.consName.c_str(), it->second.fireStrength);
    printf("------------------------------------------\n");
}
*********************Behaviour.h*********************
#ifndef BEHAVIOUR_H
#define BEHAVIOUR_H

#include "GenericHeader.h"
#include "MatchingRuleBase.h"
#include "Trapezoid.h"


class Behaviour
{
    public:
        Behaviour();

        void membershipTest(float width, float height);


        // 9 element array to store low-medium-high membership values for width and height
        double * membershipValues;


        MatchingRuleBase ruleBase;

        // trapezoids for input fuzzy variables
        Trapezoid sl_lowTrapezoid;//Sepal length
        Trapezoid sl_mediumTrapezoid;
        Trapezoid sl_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid sw_lowTrapezoid;//Sepal width
        Trapezoid sw_mediumTrapezoid;
        Trapezoid sw_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid pl_lowTrapezoid;//Petal length
        Trapezoid pl_mediumTrapezoid;
        Trapezoid pl_highTrapezoid;
        // trapezoids for input fuzzy variables
        Trapezoid pw_lowTrapezoid;//Petal width
        Trapezoid pw_mediumTrapezoid;
        Trapezoid pw_highTrapezoid;
        pw_highTrapezoid.(25);
        int result;
        string resultString;

};

#endif // BEHAVIOUR_H
*********************Behaviour.cpp*********************
#include "Behaviour.h"

Behaviour::Behaviour()
{
    // define the input membership functions with the trapezoids
    distanceLowTrapezoid.a = ...
    distanceLowTrapezoid.b =...
    distanceLowTrapezoid.c = ...
    distanceLowTrapezoid.d = ...

...


    distanceHighTrapezoid.a = ...
    distanceHighTrapezoid.b = ...
    distanceHighTrapezoid.c = ...
    distanceHighTrapezoid.d = ...


    ruleBase.setName("Classification model");

    // add rules
    // read sample rule 1 as if width is LOW and height is LOW then the type is SETOSA

    // note that sample rules may not be correct.

    // LOW
    ruleBase.addRule(MatchingRule(LOW, LOW, SETOSA));
    ruleBase.addRule(MatchingRule(LOW, MED, VIRGINICA));

    ....

    ruleBase.print();


    // fuzzy membership values for input parameters
    int ruleBaseSize =
    distanceValues = (double *)malloc(2*sizeof(double));
}

void Behaviour::membershipTest(float width, float height)
{
    // clear all fuzzy membership values (for now we have low, med and high for width and height so 3 x 3 = 9 locations)
    for(int i=0;i<10;i++)
    {
        membershipValues[i] = 0.0;
    }

    // clear previous fire strengths
    ruleBase.clearFires();

    // compute all membership functions for all combinations of LOW MEDIUM and HIGH

    // width
    // low
    if(distance <= distanceLowTrapezoid.c)
        distanceValues[LOW] = 1.0;
    else if(distanceLowTrapezoid.c < distance && distance < distanceLowTrapezoid.d)
        distanceValues[LOW] = (distanceLowTrapezoid.d - distance) / (distanceLowTrapezoid.d - distanceLowTrapezoid.c);
    else if(distanceLowTrapezoid.d <= distance)
        distanceValues[LOW] = 0.0;

    // medium
    ...


    // high
    ...

    for(int i=0;i<2;i++)
    {
        ruleBase.ruleMap[i].fireStrength = distanceValues[i];

        if(ruleBase.ruleMap[i].fireStrength > 0.0)
        {
            ruleBase.ruleMap[i].fire = true;
        }
        else
        {
            ruleBase.ruleMap[i].fire = false;
        }
    }

    // stores the pair which has the maximum fire strength

    matchingResult = ruleBase.ruleMap[result].consequent;

    matchingResultName = ruleBase.ruleMap[result].consName;

    cout<<matchingResultName<<endl;
}
*********************Trapesoid.h*********************
#ifndef TRAPEZOID_H
#define TRAPEZOID_H


class Trapezoid
{
    public:
        Trapezoid();
        Trapezoid(double aV, double bV, double cV, double dV);
        double CalcValue(double in);

        double a,b,c,d;
};
Trapezoid::Trapezoid(double aV, double bV, double cV, double dV)
{
    a=aV;
    b=bV;
    c=cV;
    d=dV;
}

double Trapezoid::CalcValue(double in)
{
    if(in<=a)
        return 0;
    if(in>a && in<b)
        return 1/(b-a)*(in-a);
    if(in>=b && in<=c)
        return 1;
    if(in>c && in<d)
        return 1/(c-d)*(in-d);
    if(in>=d)
        return 0;
}

#endif // TRAPEZOID_H
*********************Trapezoid.cpp********************
#include "Trapezoid.h"

Trapezoid::Trapezoid()
{
    a=b=c=d=0.0;
}

Trapezoid::Trapezoid(double aV, double bV, double cV, double dV)
{
    a = aV;
    b = bV;
    c = cV;
    d = dV;
}
*********************Tuple2.h*********************
#ifndef TUPLE2_H
#define TUPLE2_H

class Tuple2
{
public:
    Tuple2();
    Tuple2(int k1, int k2);
    bool operator<(const Tuple2 &right) const;
    int key1, key2;
};

#endif
*********************Tuple2.cpp*********************
#include "Tuple2.h"

Tuple2::Tuple2()
{

}
Tuple3::Tuple3(int k1, int k2)
{
    key1 = k1;
    key2 = k2;
}

bool Tuple2:: operator<(const Tuple2 &right) const
{
    if ( key1 == right.key1 )
    {
        if ( key2 == right.key2 )
            return true;
        else
            return key2 < right.key2;
     }
     else
         return key1 < right.key1;

}
*********************GenericHeader.h*********************
#ifndef GENERICHEADER_H
#define	GENERICHEADER_H

#include <iostream>
#include <map>
#include <list>
#include <vector>

#include <stdlib.h>
#include <stdio.h>



using namespace std;


// definitions for fuzzy sets
// inputs
#define LOW 0
#define MED 1
#define HIGH 2

// output strings
#define LOWs "LOW"
#define MEDs "MEDIUM"
#define HIGHs "HIGH"


// outputs
#define SETOSA 0
#define VERSICOLOR 1
#define VIRGINICA 2

// output strings
#define SETOSAs "Iris Setosa"
#define VERSICOLORs "Iris Versicolor"
#define VIRGINICAs "Iris Virginica"

#define UNDF "UNDEFINED"


#endif	/* GENERICHEADER_H */

